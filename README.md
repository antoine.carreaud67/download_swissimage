# Download Swissimage from a CSV file

## Install

open a terminal, make sure poetry is installed

'poetry install'

## Download images from the csv

Make sure the csv file from https://www.swisstopo.admin.ch/fr/orthophotos-swissimage-10-cm is in the dir

'poetry run python download.py /path/to/file.csv'

All swissimages will be downloaded in the data dir