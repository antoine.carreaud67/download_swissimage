import os
import sys
import pandas as pd
import requests


def telecharger_donnees(url, destination, count):
    response = requests.get(url)
    if response.status_code == 200:
        with open(destination, 'wb') as f:
            f.write(response.content)
        #print(f"Image downloaded : {url}")
        count+=1
    else:
        print(f"Failed : {url}. Status : {response.status_code}")
        with open('failed_images.txt','w') as f:
            f.wirte(url+'\n')
    return count


def main():
    csv_file = sys.argv[1]
    df = pd.read_csv (csv_file, header=None)
    count_download = 0
    count = 0
    total = len(df)
    print(f'Downloading {total} images.')
    if not os.path.exists('data'):
        os.mkdir('data')
    for row in df.itertuples():
        download_link = row[1]
        count+=1
        if count%10==0:
            print(f'Process at {count/total*100:.1f} %')
        fn = download_link.split('/')[-1]
        fn_local = os.path.join('data', fn)
        count_download = telecharger_donnees(download_link, fn_local, count_download)
    print(f'Process finished with {count_download} images downloaded out of {total} planned')

if __name__ == "__main__":
    main()
